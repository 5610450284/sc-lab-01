package Controll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import View.Gui;
import Model.Card;

public class TestCase {
	
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		new TestCase();
	}

	public TestCase() {
		frame = new Gui();
		frame.pack();
		frame.setVisible(true);
		//frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		frame.setBounds(500, 500, 400, 400);
		setTestCase();
	}

	public void setTestCase() {
		Card card = new Card();
		card.addMoney(500);
		frame.setResult(card.toString());
		card.checkBalance();
		frame.extendResult(card.toBalance());
		card.purchace(150);;
		frame.extendResult(card.toString());
		card.checkBalance();
		frame.extendResult(card.toBalance());
	}

	ActionListener list;
	Gui frame;

}
